<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Integration;

use Altek\Accountant\Context;
use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Events\Recording;
use Altek\Accountant\Exceptions\AccountantException;
use Altek\Accountant\Models\Ledger;
use Altek\Accountant\Tests\AccountantTestCase;
use Altek\Accountant\Tests\Database\Factories\ArticleFactory;
use Altek\Accountant\Tests\Database\Factories\UserFactory;
use Altek\Accountant\Tests\Models\Article;
use Altek\Accountant\Tests\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use InvalidArgumentException;

class RecordingTest extends AccountantTestCase
{
    /**
     * @test
     */
    public function itWillNotRecordWhenInTestContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::CLI | Context::WEB);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(0, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillNotRecordWhenInCliContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::TEST | Context::WEB);

        App::shouldReceive('runningUnitTests')
            ->andReturn(false);

        App::shouldReceive('runningInConsole')
            ->andReturn(true);

        UserFactory::new()->create();


        self::assertSame(1, User::count());
        self::assertSame(0, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillNotRecordWhenInWebContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::TEST | Context::CLI);

        App::shouldReceive('runningUnitTests')
            ->andReturn(false);

        App::shouldReceive('runningInConsole')
            ->andReturn(false);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(0, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillNotRecordInAnyContext(): void
    {
        $this->app['config']->set('accountant.contexts', 0b000);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(0, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillRecordWhenInTestContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::TEST);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(1, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillRecordWhenInCliContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::CLI);

        App::shouldReceive('runningUnitTests')
            ->andReturn(false);

        App::shouldReceive('runningInConsole')
            ->andReturn(true);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(1, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillRecordWhenInWebContext(): void
    {
        $this->app['config']->set('accountant.contexts', Context::WEB);

        App::shouldReceive('runningUnitTests')
            ->andReturn(false);

        App::shouldReceive('runningInConsole')
            ->andReturn(false);

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(1, Ledger::count());
    }

    /**
     * @test
     */
    public function itWillNotRecordTheRetrievedEventByDefault(): void
    {
        self::assertSame(0, User::count());
        self::assertSame(0, Ledger::count());

        UserFactory::new()->create();

        self::assertSame(1, User::count());
        self::assertSame(1, Ledger::count());

        User::first();

        self::assertSame(1, Ledger::count());
        self::assertSame(1, User::count());
    }

    /**
     * @test
     */
    public function itWillRecordTheRetrievedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'retrieved',
        ]);

        ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
        ]);

        self::assertSame(0, Ledger::count());

        Article::first();

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('retrieved', $ledger->event);

        self::assertSame([
            'id'           => '1',
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'reviewed'     => '0',
            'published_at' => null,
            'created_at'   => '2012-06-14 15:03:03',
            'updated_at'   => '2012-06-14 15:03:03',
            'deleted_at'   => null,
        ], $ledger->properties);

        self::assertEmpty($ledger->modified);
    }

    /**
     * @test
     */
    public function itWillRecordTheCreatedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'created',
        ]);

        self::assertSame(0, Ledger::count());

        ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('created', $ledger->event);

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
        ], $ledger->properties);

        self::assertSame([
            'title',
            'content',
            'published_at',
            'reviewed',
            'updated_at',
            'created_at',
            'id',
        ], $ledger->modified);
    }

    /**
     * @test
     */
    public function itWillRecordTheUpdatedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'updated',
        ]);

        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
        ]);

        self::assertSame(0, Ledger::count());

        $article->update([
            'content'      => 'First step: install the Accountant package.',
            'published_at' => Carbon::now(),
            'reviewed'     => 1,
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('updated', $ledger->event);

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'published_at' => '2012-06-14 15:03:03',
            'reviewed'     => 1,
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
        ], $ledger->properties);

        self::assertSame([
            'content',
            'published_at',
            'reviewed',
        ], $ledger->modified);
    }

    /**
     * @test
     */
    public function itWillRecordTheRestoredEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'restored',
        ]);

        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'deleted_at'   => Carbon::now(),
        ]);

        $article->restore();

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('restored', $ledger->event);

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'deleted_at'   => null,
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
        ], $ledger->properties);

        self::assertEmpty($ledger->modified);
    }

    /**
     * @test
     */
    public function itWillRecordTheDeletedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'deleted',
        ]);

        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
        ]);

        self::assertSame(0, Ledger::count());

        $article->delete();

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('deleted', $ledger->event);

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
            'deleted_at'   => '2012-06-14 15:03:03',
        ], $ledger->properties);

        self::assertEmpty($ledger->modified);

        // Make sure the trashed record is retrievable
        self::assertInstanceOf(Article::class, $ledger->recordable);
    }

    /**
     * @test
     */
    public function itWillRecordTheForceDeletedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'forceDeleted',
        ]);

        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'deleted_at'   => Carbon::now(),
        ]);

        $article->forceDelete();

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('forceDeleted', $ledger->event);

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'deleted_at'   => '2012-06-14 15:03:03',
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
        ], $ledger->properties);

        self::assertEmpty($ledger->modified);

        // The (force) deleted record is not retrievable
        self::assertNull($ledger->recordable);
    }

    /**
     * @test
     */
    public function itWillRecordTheToggledEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'toggled',
            'attached',
            'detached',
        ]);

        $user = UserFactory::new()->create();
        ArticleFactory::new()->count(2)->create();

        self::assertSame(0, Ledger::count());

        $user->articles()->sync([
            2 => [
                'liked' => false,
            ],
        ]);

        $user->articles()->toggle([
            2 => [
                'liked' => false,
            ],
            1 => [
                'liked' => true,
            ],
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('toggled', $ledger->event);

        self::assertEmpty($ledger->modified);

        self::assertSame([
            'relation'   => 'articles',
            'properties' => [
                [
                    'user_id'    => 1,
                    'liked'      => false,
                    'article_id' => 2,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
            ],
        ], $ledger->getPivotData());
    }

    /**
     * @test
     */
    public function itWillRecordTheSyncedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'synced',
            'existingPivotUpdated',
            'attached',
            'detached',
        ]);

        $user = UserFactory::new()->create();
        ArticleFactory::new()->count(3)->create();

        $user->articles()->toggle([
            2 => [
                'liked' => true,
            ],
            3 => [
                'liked' => false,
            ],
        ]);

        $user->articles()->sync([
            2 => [
                'liked' => false,
            ],
            1 => [
                'liked' => true,
            ],
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('synced', $ledger->event);

        self::assertEmpty($ledger->modified);

        self::assertSame([
            'relation'   => 'articles',
            'properties' => [
                [
                    'user_id'    => 1,
                    'liked'      => false,
                    'article_id' => 2,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
            ],
        ], $ledger->getPivotData());
    }

    /**
     * @test
     */
    public function itWillRecordTheExistingPivotUpdatedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'existingPivotUpdated',
        ]);

        $user = UserFactory::new()->create();

        $articles = ArticleFactory::new()->count(2)->create()->each(static function (Article $article) use ($user): void {
            $article->users()->attach($user, [
                'liked' => false,
            ]);
        });

        self::assertSame(0, Ledger::count());

        $user->articles()->updateExistingPivot($articles, [
            'liked' => true,
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('existingPivotUpdated', $ledger->event);

        self::assertEmpty($ledger->modified);

        self::assertSame([
            'relation'   => 'articles',
            'properties' => [
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 2,
                ],
            ],
        ], $ledger->getPivotData());
    }

    /**
     * @test
     */
    public function itWillRecordTheAttachedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'attached',
        ]);

        $user = UserFactory::new()->create();
        ArticleFactory::new()->count(2)->create();

        self::assertSame(0, Ledger::count());

        $user->articles()->attach([
            2 => [
                'liked' => false,
            ],
            1 => [
                'liked' => true,
            ],
        ]);

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('attached', $ledger->event);

        self::assertEmpty($ledger->modified);

        self::assertSame([
            'relation'   => 'articles',
            'properties' => [
                [
                    'user_id'    => 1,
                    'liked'      => false,
                    'article_id' => 2,
                ],
                [
                    'user_id'    => 1,
                    'liked'      => true,
                    'article_id' => 1,
                ],
            ],
        ], $ledger->getPivotData());
    }

    /**
     * @test
     */
    public function itWillRecordTheDetachedEvent(): void
    {
        $this->app['config']->set('accountant.events', [
            'detached',
        ]);

        $user = UserFactory::new()->create();

        ArticleFactory::new()->count(2)->create()->each(static function (Article $article) use ($user): void {
            $article->users()->attach($user, [
                'liked' => $article->getKey() === 1,
            ]);
        });

        self::assertSame(0, Ledger::count());

        $user->articles()->detach();

        self::assertSame(1, Ledger::count());

        $ledger = Ledger::first();

        self::assertSame('detached', $ledger->event);

        self::assertEmpty($ledger->modified);

        self::assertSame([
            'relation'   => 'articles',
            'properties' => [
                [
                    'user_id'    => 1,
                    'article_id' => '1',
                ],
                [
                    'user_id'    => 1,
                    'article_id' => '2',
                ],
            ],
        ], $ledger->getPivotData());
    }

    /**
     * @test
     */
    public function itWillKeepAllLedgers(): void
    {
        $this->app['config']->set('accountant.ledger.threshold', 0);
        $this->app['config']->set('accountant.events', [
            'updated',
        ]);

        $article = ArticleFactory::new()->create([
            'reviewed' => 1,
        ]);

        foreach (\range(0, 99) as $count) {
            $article->update([
                'reviewed' => $count % 2,
            ]);
        }

        self::assertSame(100, $article->ledgers()->count());
    }

    /**
     * @test
     */
    public function itWillRemoveOlderLedgersAboveTheThreshold(): void
    {
        $this->app['config']->set('accountant.ledger.threshold', 10);
        $this->app['config']->set('accountant.events', [
            'updated',
        ]);

        $article = ArticleFactory::new()->create([
            'reviewed' => 1,
        ]);

        foreach (\range(0, 99) as $count) {
            $article->update([
                'reviewed' => $count % 2,
            ]);
        }

        self::assertSame(10, $article->ledgers()->count());
    }

    /**
     * @test
     */
    public function itWillNotRecordDueToUnsupportedDriver(): void
    {
        $this->app['config']->set('accountant.ledger.driver', 'foo');

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Driver [foo] not supported.');

        ArticleFactory::new()->create();
    }

    /**
     * @test
     */
    public function itWillNotRecordDueToClassNotImplementingDriverInterface(): void
    {
        // We just pass a FQCN that does not implement the LedgerDriver interface
        $this->app['config']->set('accountant.ledger.driver', self::class);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('The LedgerDriver contract must be implemented by the driver');

        ArticleFactory::new()->create();
    }

    /**
     * @test
     */
    public function itWillNotRecordDueToClassNotImplementingLedgerInterface(): void
    {
        // We just pass a FQCN that does not implement the Ledger interface
        $this->app['config']->set('accountant.ledger.implementation', self::class);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid Ledger implementation: "Altek\Accountant\Tests\Integration\RecordingTest"');

        ArticleFactory::new()->create();
    }

    /**
     * @test
     */
    public function itWillNotRecordDueToClassNotImplementingNotaryInterface(): void
    {
        // We just pass a FQCN that does not implement the Notary interface
        $this->app['config']->set('accountant.notary', self::class);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid Notary implementation: "Altek\Accountant\Tests\Integration\RecordingTest"');

        ArticleFactory::new()->create();
    }

    /**
     * @test
     */
    public function itWillRecordUsingTheDefaultDriver(): void
    {
        $this->app['config']->set('accountant.ledger.driver', null);

        ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes Using The Fallback Driver',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
        ]);

        $ledger = Ledger::first();

        self::assertSame([
            'title'        => 'Keeping Track Of Eloquent Model Changes Using The Fallback Driver',
            'content'      => 'N/A',
            'published_at' => null,
            'reviewed'     => 0,
            'updated_at'   => '2012-06-14 15:03:03',
            'created_at'   => '2012-06-14 15:03:03',
            'id'           => 1,
        ], $ledger->properties);

        self::assertSame([
            'title',
            'content',
            'published_at',
            'reviewed',
            'updated_at',
            'created_at',
            'id',
        ], $ledger->modified);
    }

    /**
     * @test
     */
    public function itWillCancelTheLedgerCreationFromTheEventListener(): void
    {
        Event::listen(Recording::class, static function (Recording $recording) {
            self::assertObjectHasAttribute('model', $recording);
            self::assertObjectHasAttribute('driver', $recording);
            self::assertObjectHasAttribute('event', $recording);
            self::assertObjectHasAttribute('pivotRelation', $recording);
            self::assertObjectHasAttribute('pivotProperties', $recording);

            return false;
        });

        ArticleFactory::new()->create();

        self::assertNull(Ledger::first());
    }

    /**
     * @test
     */
    public function itDisablesAndEnablesRecordingBackAgain(): void
    {
        // Recording is enabled by default
        self::assertTrue(Article::$recordingEnabled);

        ArticleFactory::new()->create();

        self::assertSame(1, Article::count());
        self::assertSame(1, Ledger::count());

        // Disable Recording
        Article::disableRecording();
        self::assertFalse(Article::$recordingEnabled);

        ArticleFactory::new()->create();

        self::assertSame(2, Article::count());
        self::assertSame(1, Ledger::count());

        // Re-enable Recording
        Article::enableRecording();
        self::assertTrue(Article::$recordingEnabled);

        ArticleFactory::new()->create();

        self::assertSame(2, Ledger::count());
        self::assertSame(3, Article::count());
    }

    /**
     * @test
     */
    public function itSuccessfullyVerifiesExtractedLedgerData(): void
    {
        $user = UserFactory::new()->create([
            'is_admin'   => 1,
            'first_name' => 'rick',
            'last_name'  => 'Sanchez',
            'email'      => 'rick@wubba-lubba-dub.dub',
        ]);

        $ledger = Ledger::first();

        $extractedUser = $ledger->extract();

        self::assertTrue($user->is($extractedUser));

        self::assertInstanceOf(Recordable::class, $extractedUser);
        self::assertInstanceOf(User::class, $extractedUser);

        self::assertSame([
            'is_admin'   => true,
            'first_name' => 'Rick',
            'last_name'  => 'Sanchez',
            'email'      => 'rick@wubba-lubba-dub.dub',
            'updated_at' => '2012-06-14T15:03:03.000000Z',
            'created_at' => '2012-06-14T15:03:03.000000Z',
            'id'         => 1,
            'full_name'  => 'Rick Sanchez',
        ], $extractedUser->toArray());
    }
}
