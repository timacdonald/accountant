<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Unit;

use Altek\Accountant\Ciphers\Base64;
use Altek\Accountant\Ciphers\Bleach;
use Altek\Accountant\Context;
use Altek\Accountant\Contracts\Identifiable;
use Altek\Accountant\Exceptions\AccountantException;
use Altek\Accountant\Tests\AccountantTestCase;
use Altek\Accountant\Tests\Database\Factories\ArticleFactory;
use Altek\Accountant\Tests\Database\Factories\LedgerFactory;
use Altek\Accountant\Tests\Database\Factories\UserFactory;
use Altek\Accountant\Tests\Models\Article;
use Altek\Accountant\Tests\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class RecordableTest extends AccountantTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();

        // Clear morph maps
        Relation::morphMap([], false);
    }

    /**
     * @group Recordable::shouldRegisterObserver
     * @test
     *
     * @dataProvider contextResolverProvider
     *
     * @param int  $contexts
     * @param bool $test
     * @param bool $cli
     */
    public function itWillNotRegisterTheRecordableObserverWhenNotInContext(int $contexts, bool $test, bool $cli): void
    {
        $this->app['config']->set('accountant.contexts', $contexts);

        App::shouldReceive('runningUnitTests')
            ->andReturn($test);

        App::shouldReceive('runningInConsole')
            ->andReturn($cli);

        self::assertFalse(Article::shouldRegisterObserver());
    }

    /**
     * @return array
     */
    public function contextResolverProvider(): array
    {
        return [
            'In Test with CLI and Web allowed' => [
                Context::CLI | Context::WEB,

                // Test
                true,
                false,
            ],

            'In CLI with Test and Web allowed' => [
                Context::TEST | Context::WEB,

                // CLI
                false,
                true,
            ],

            'In Web with Test and CLI allowed' => [
                Context::TEST | Context::CLI,

                // Web
                false,
                false,
            ],

            'In Test with no context allowed' => [
                0,

                // Test
                true,
                false,
            ],

            'In CLI with no context allowed' => [
                0,

                // CLI
                false,
                true,
            ],

            'In Web with no context allowed' => [
                0,

                // Web
                false,
                false,
            ],
        ];
    }

    /**
     * @group Recordable::shouldRegisterObserver
     * @test
     */
    public function itWillNotRegisterTheRecordableObserverDueToClassNotImplementingContextResolverInterface(): void
    {
        $this->app['config']->set('accountant.resolvers.context', self::class);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid ContextResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        Article::shouldRegisterObserver();
    }

    /**
     * @group Recordable::disableRecording
     * @group Recordable::shouldRegisterObserver
     * @test
     */
    public function itWillNotRegisterTheRecordableObserverWhenRecordingIsDisabled(): void
    {
        Article::disableRecording();

        self::assertFalse(Article::shouldRegisterObserver());

        Article::enableRecording();
    }

    /**
     * @group Recordable::shouldRegisterObserver
     * @test
     */
    public function itWillRegisterTheRecordableObserverByDefault(): void
    {
        self::assertTrue(Article::shouldRegisterObserver());
    }

    /**
     * @group Recordable::getRecordableEvents
     * @test
     */
    public function itReturnsTheDefaultLedgerEvents(): void
    {
        $article = new Article();

        self::assertSame([
            'created',
            'updated',
            'restored',
            'deleted',
            'forceDeleted',
        ], $article->getRecordableEvents());
    }

    /**
     * @group Recordable::getRecordableEvents
     * @test
     */
    public function itReturnsTheCustomLedgerEventsFromAttribute(): void
    {
        $article = new Article();

        $article->recordableEvents = [
            'deleted',
            'restored',
        ];

        self::assertSame([
            'deleted',
            'restored',
        ], $article->getRecordableEvents());
    }

    /**
     * @group Recordable::getRecordableEvents
     * @test
     */
    public function itReturnsTheCustomLedgerEventsFromConfig(): void
    {
        $this->app['config']->set('accountant.events', [
            'deleted',
            'restored',
        ]);

        $article = new Article();

        self::assertSame([
            'deleted',
            'restored',
        ], $article->getRecordableEvents());
    }

    /**
     * @group Recordable::isEventRecordable
     * @test
     */
    public function itIsNotARecordableEvent(): void
    {
        $article = new Article();

        self::assertFalse($article->isEventRecordable('retrieved'));
    }

    /**
     * @group Recordable::isEventRecordable
     * @test
     */
    public function itIsARecordableEvent(): void
    {
        $article = new Article();

        $article->recordableEvents = [
            'retrieved',
            'created',
            'updated',
            'restored',
            'deleted',
            'forceDeleted',
            'toggle',
            'sync',
            'existingPivotUpdated',
            'attached',
            'detached',
        ];

        self::assertTrue($article->isEventRecordable('retrieved'));
        self::assertTrue($article->isEventRecordable('created'));
        self::assertTrue($article->isEventRecordable('updated'));
        self::assertTrue($article->isEventRecordable('restored'));
        self::assertTrue($article->isEventRecordable('deleted'));
        self::assertTrue($article->isEventRecordable('forceDeleted'));
        self::assertTrue($article->isEventRecordable('toggle'));
        self::assertTrue($article->isEventRecordable('sync'));
        self::assertTrue($article->isEventRecordable('existingPivotUpdated'));
        self::assertTrue($article->isEventRecordable('attached'));
        self::assertTrue($article->isEventRecordable('detached'));
    }

    /**
     * @group Recordable::gather
     * @group Recordable::disableRecording
     * @test
     */
    public function itFailsTogatherDataWhenRecordingIsNotEnabled(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Recording is not enabled');

        $article = new Article();

        $article::disableRecording();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @group Recordable::enableRecording
     * @test
     */
    public function itFailsTogatherDataWhenAnInvalidLedgerEventIsPassed(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid event: "retrieved"');

        $article = new Article();

        $article::enableRecording();

        $article->gather('retrieved');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenTheContextResolverImplementationIsInvalid(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid ContextResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $this->app['config']->set('accountant.resolvers.context', self::class);

        $article = new Article();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenTheIpAddressResolverImplementationIsInvalid(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid IpAddressResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $this->app['config']->set('accountant.resolvers.ip_address', self::class);

        $article = new Article();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenTheUrlResolverImplementationIsInvalid(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid UrlResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $this->app['config']->set('accountant.resolvers.url', self::class);

        $article = new Article();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenTheUserAgentResolverImplementationIsInvalid(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid UserAgentResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $this->app['config']->set('accountant.resolvers.user_agent', self::class);

        $article = new Article();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenTheUserResolverImplementationIsInvalid(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid UserResolver implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $this->app['config']->set('accountant.resolvers.user', self::class);

        $article = new Article();

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itSuccessfullyReturnsThegatheredDataForRecording(): void
    {
        $article = ArticleFactory::new()->make([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ]);

        self::assertCount(12, $data = $article->gather('created'));

        self::assertSame([
            'user_id'         => null,
            'user_type'       => null,
            'context'         => Context::TEST,
            'event'           => 'created',
            'recordable_id'   => null,
            'recordable_type' => Article::class,
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-14 15:03:03',
                'reviewed'     => 1,
            ],
            'modified' => [
                'title',
                'content',
                'published_at',
                'reviewed',
            ],
            'extra'      => [],
            'url'        => 'Command Line Interface',
            'ip_address' => '127.0.0.1',
            'user_agent' => 'Symfony',
        ], $data);
    }

    /**
     * @group Recordable::gather
     * @test
     *
     * @dataProvider userResolverProvider
     *
     * @param string $guard
     * @param string $driver
     * @param int    $id
     * @param string $type
     */
    public function itSuccessfullyReturnsgatheredDataForRecordingIncludingResolvedUser(
        string $guard,
        string $driver,
        int $id = null,
        string $type = null
    ): void {
        $this->app['config']->set('accountant.user.guards', [
            $guard,
        ]);

        $user = UserFactory::new()->create();

        $this->actingAs($user, $driver);

        $article = ArticleFactory::new()->make([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ]);

        self::assertCount(12, $data = $article->gather('created'));

        self::assertSame([
            'user_id'         => $id,
            'user_type'       => $type,
            'context'         => Context::TEST,
            'event'           => 'created',
            'recordable_id'   => null,
            'recordable_type' => Article::class,
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-14 15:03:03',
                'reviewed'     => 1,
            ],
            'modified' => [
                'title',
                'content',
                'published_at',
                'reviewed',
            ],
            'extra'      => [],
            'url'        => 'Command Line Interface',
            'ip_address' => '127.0.0.1',
            'user_agent' => 'Symfony',
        ], $data);
    }

    /**
     * @return array
     */
    public function userResolverProvider(): array
    {
        return [
            [
                'api',
                'web',
                null,
                null,
            ],
            [
                'web',
                'api',
                null,
                null,
            ],
            [
                'api',
                'api',
                1,
                User::class,
            ],
            [
                'web',
                'web',
                1,
                User::class,
            ],
        ];
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itSuccessfullyReturnsgatheredDataIncludingExtraSupply(): void
    {
        $article = new class() extends Article {
            public function supplyExtra(string $event, array $properties, ?Identifiable $user): array
            {
                return [
                    'slug' => Str::slug($properties['title']),
                ];
            }
        };

        $article->setRawAttributes([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => '2012-06-14 15:03:03',
        ]);

        self::assertCount(12, $data = $article->gather('created'));

        self::assertSame([
            'user_id'         => null,
            'user_type'       => null,
            'context'         => Context::TEST,
            'event'           => 'created',
            'recordable_id'   => null,
            'recordable_type' => \get_class($article),
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => 'First step: install the Accountant package.',
                'reviewed'     => 1,
                'published_at' => '2012-06-14 15:03:03',
            ],
            'modified' => [
                'title',
                'content',
                'reviewed',
                'published_at',
            ],
            'extra' => [
                'slug' => 'keeping-track-of-eloquent-model-changes',
            ],
            'url'        => 'Command Line Interface',
            'ip_address' => '127.0.0.1',
            'user_agent' => 'Symfony',
        ], $data);
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenUsingInvalidCipherProperty(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid property: "invalid_property"');

        $article = ArticleFactory::new()->make();

        $article->ciphers = [
            'invalid_property' => Base64::class,
        ];

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itFailsTogatherDataWhenUsingInvalidCipherImplementation(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid Cipher implementation: "Altek\Accountant\Tests\Unit\RecordableTest"');

        $article = ArticleFactory::new()->make();

        $article->ciphers = [
            'title' => self::class,
        ];

        $article->gather('created');
    }

    /**
     * @group Recordable::gather
     * @test
     */
    public function itSuccessfullyCiphersThegatheredData(): void
    {
        $article = ArticleFactory::new()->make([
            'title'        => 'Keeping Track Of Models',
            'content'      => 'N/A',
            'reviewed'     => 0,
            'published_at' => null,
        ]);

        $article->syncOriginal();

        $article->title        = 'Keeping Track Of Eloquent Model Changes';
        $article->content      = 'First step: install the Accountant package.';
        $article->published_at = Carbon::now();
        $article->reviewed     = 1;

        $article->ciphers = [
            'content'  => Bleach::class,
            'reviewed' => Base64::class,
        ];

        self::assertCount(12, $data = $article->gather('updated'));

        self::assertSame([
            'user_id'         => null,
            'user_type'       => null,
            'context'         => Context::TEST,
            'event'           => 'updated',
            'recordable_id'   => null,
            'recordable_type' => Article::class,
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => '--------------------------------------kage.',
                'published_at' => '2012-06-14 15:03:03',
                'reviewed'     => 'MQ==',
                'ciphers'      => [
                    'content'  => Bleach::class,
                    'reviewed' => Base64::class,
                ],
            ],
            'modified' => [
                'title',
                'content',
                'published_at',
                'reviewed',
                'ciphers',
            ],
            'extra'      => [],
            'url'        => 'Command Line Interface',
            'ip_address' => '127.0.0.1',
            'user_agent' => 'Symfony',
        ], $data);
    }

    /**
     * @group Recordable::getLedgerDriver
     * @test
     */
    public function itReturnsTheDefaultLedgerDriverValue(): void
    {
        $article = new Article();

        self::assertSame('database', $article->getLedgerDriver());
    }

    /**
     * @group Recordable::getLedgerDriver
     * @test
     */
    public function itReturnsTheCustomLedgerDriverValueFromAttribute(): void
    {
        $article = new Article();

        $article->ledgerDriver = 'RedisDriver';

        self::assertSame('RedisDriver', $article->getLedgerDriver());
    }

    /**
     * @group Recordable::getLedgerDriver
     * @test
     */
    public function itReturnsTheCustomLedgerDriverValueFromConfig(): void
    {
        $this->app['config']->set('accountant.ledger.driver', 'RedisDriver');

        $article = new Article();

        self::assertSame('RedisDriver', $article->getLedgerDriver());
    }

    /**
     * @group Recordable::getLedgerThreshold
     * @test
     */
    public function itReturnsTheDefaultLedgerThresholdValue(): void
    {
        $article = new Article();

        self::assertSame(0, $article->getLedgerThreshold());
    }

    /**
     * @group Recordable::getLedgerThreshold
     * @test
     */
    public function itReturnsTheCustomLedgerThresholdValueFromAttribute(): void
    {
        $article = new Article();

        $article->ledgerThreshold = 10;

        self::assertSame(10, $article->getLedgerThreshold());
    }

    /**
     * @group Recordable::getLedgerThreshold
     * @test
     */
    public function itReturnsTheCustomLedgerThresholdValueFromConfig(): void
    {
        $this->app['config']->set('accountant.ledger.threshold', 200);

        $article = new Article();

        self::assertSame(200, $article->getLedgerThreshold());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToMissingTimestamps(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('The use of timestamps is required');

        $article = ArticleFactory::new()->make();

        $article->timestamps = false;

        $article->isCurrentStateReachable();
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToMissingLedgers(): void
    {
        $article = ArticleFactory::new()->make();

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToCreatedEventMissingFromFirstLedger(): void
    {
        $this->app['config']->set('accountant.events', [
            'updated',
        ]);

        $article = ArticleFactory::new()->create();

        $article->update([
            'title' => 'A change was made to the title',
        ]);

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToCreatedAtValueMismatch(): void
    {
        $this->app['config']->set('accountant.events', []);

        $article = ArticleFactory::new()->create();

        LedgerFactory::new()->create([
            'event'           => 'created',
            'recordable_type' => Article::class,
            'recordable_id'   => $article->getKey(),
            'properties'      => [
                'created_at' => '2015-10-24 23:11:10',
            ],
        ]);

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToUpdatedAtValueMismatch(): void
    {
        $this->app['config']->set('accountant.events', []);

        $article = ArticleFactory::new()->create();

        LedgerFactory::new()->create([
            'event'           => 'created',
            'recordable_type' => Article::class,
            'recordable_id'   => $article->getKey(),
            'properties'      => [
                'created_at' => '2012-06-14 15:03:03',
                'updated_at' => '2015-10-24 23:11:10',
            ],
        ]);

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToATaintedLedger(): void
    {
        $article = ArticleFactory::new()->create();

        // Taint the Ledger
        $ledger           = $article->ledgers()->first();
        $ledger->modified = [
            'title',
        ];

        $ledger->save();

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itFailsToValidateTheCurrentStateDueToPropertyMismatch(): void
    {
        $this->app['config']->set('accountant.events', [
            'created',
        ]);

        $article = ArticleFactory::new()->create();

        $article->update([
            'content' => 'A change was made to the content',
        ]);

        self::assertFalse($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itSuccessfullyValidatesTheCurrentState(): void
    {
        $article = ArticleFactory::new()->create();

        $article->update([
            'content' => 'A change was made to the content',
        ]);

        $article->update([
            'title' => 'A change was made to the title, too!',
        ]);

        self::assertTrue($article->isCurrentStateReachable());
    }

    /**
     * @group Recordable::isCurrentStateReachable
     * @test
     */
    public function itSuccessfullyValidatesTheCurrentStateWhileIgnoringNonModifyingEvents(): void
    {
        $this->app['config']->set('accountant.events', [
            'created',
            'updated',
            'retrieved',
        ]);

        $article = ArticleFactory::new()->create();

        $article->update([
            'content' => 'A change was made to the content',
        ]);

        Article::first();

        $article->update([
            'title' => 'A change was made to the title, too!',
        ]);

        self::assertTrue($article->isCurrentStateReachable());
    }
}
